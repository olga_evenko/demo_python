from django.http import JsonResponse


def get_error_description(val, text, code=403):
    if val == 'error_esia_login':
        if not text:
            text = 'Ошибка авторизации'
        return code, text


def get_redirect_error(request, error):
    error_code = error.get('error_code', 403)
    error_text = error.get('error_text', 'Ошибка при авторизации')
    return (request.session.get('app_redirect') or '/') + f'?error={error_code}&error_description={error_text}'


def return_exeption(text):
    return JsonResponse(
        {
            'error_fields': None,
            'error_description': text,
        }
        , status=401)
