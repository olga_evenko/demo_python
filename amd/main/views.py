import json

from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.mixins import AccessMixin
from django.http import JsonResponse, HttpResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from model_app.users.models import Token
from rest_framework.views import APIView

from statistic.views.work_space import CsrfExemptSessionAuthentication

DB_PG_APP = 'app_db'


def return_exeption(text):
    return JsonResponse(
        {
            'error_fields': None,
            'error_description': text,
        }
        , status=401)


class TokenRequiredMixin(AccessMixin):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    permission_denied_message = 'Данный эндпоинт требует наличия токена'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.token:
            return return_exeption('Данный эндпоинт требует наличия токена')
        return super().dispatch(request, *args, **kwargs)


class UserRequiredMixin(AccessMixin):
    authentication_classes = [CsrfExemptSessionAuthentication, ]
    permission_denied_message = 'Данный эндпоинт требует наличия авторизованного пользователя'
    raise_exception = True

    def dispatch(self, request, *args, **kwargs):
        if not request.user or not request.user.is_authenticated or not request.token:
            return return_exeption('Данный эндпоинт требует наличия авторизованного пользователя')

        return super().dispatch(request, *args, **kwargs)


class ErrorResponseMixin:

    def dispatch(self, request, *args, **kwargs):
        try:
            return super().dispatch(request, *args, **kwargs)
        except Exception as e:
            if len(e.args) > 1 and isinstance(e.args[0], str) and isinstance(e.args[1], str):
                label = e.args[0]
                message = e.args[1]
            else:
                label = 'system_error'
                message = e.args[0]

            return JsonResponse({
                'error': {
                    'code': label,
                    'text': message,
                }
            }, status=409)


@method_decorator(csrf_exempt, name='dispatch')
class TokenView(View):

    def post(self, request, *args, **kwargs):
        token = Token.objects.create()
        return JsonResponse({
            'token': token.token,
            'token_refresh': token.refresh,
            'token_expire': token.token_expire
        })


class TokenRefreshView(View):

    def post(self, request, *args, **kwargs):
        refresh = request.POST.get('refresh')
        if request.user and getattr(request.token, 'refresh', None) == refresh:
            token = request.token.refresh_tokens()
            return JsonResponse({
                'token': token.token,
                'token_refresh': token.refresh,
                'token_expire': token.token_expire
            })
        return return_exeption('Не удалось войти с указанными данными')


class AuthException(Exception): pass


class TokenAuthView(APIView):
    def post(self, request, *args, **kwargs):
        data = request.POST if request.POST else request.data
        username = data.get('username')
        password = data.get('password')
        force_login = data.get('force_login')
        if force_login and username and request.user.is_superuser:
            from django.contrib.auth.models import User
            from django.core.exceptions import ObjectDoesNotExist
            try:
                user = User.objects.get(username=username)
            except ObjectDoesNotExist:
                return return_exeption('Не удалось войти с указанными данными')
        else:
            try:
                if username and password:
                    user = authenticate(request=request, username=username, password=password)
                    if not user:
                        return return_exeption('Не удалось войти с указанными данными')
                else:
                    return return_exeption('Не указаны логин или пароль')
            except AuthException as e:
                return self.get_esia_login_response({
                    'code': e.args[0],
                    'text': e.args[1],
                })
        token = Token.objects.create()
        token.set_user(user)
        if data.get('loginas') == 'true':
            login(request, user)
        return JsonResponse({
            'token': token.token,
            'token_refresh': token.refresh,
            'token_expire': token.token_expire
        })


class AuthView(TokenRequiredMixin, ErrorResponseMixin, APIView):

    def get_auth_response(self, user):
        return JsonResponse({
            'user_id': user.id,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'state': 'auth',
            'company_id': user.profiles.company.id,
        })

    def get_esia_login_response(self, error=None):
        # генерим одноразовый код для обработки в ЕСИА
        return JsonResponse({
            'error': error,
            'url_follow': '%s?%s' % (
                reverse('esia-login'),
                'login_code=%s' % self.request.token.set_esia_login_code()
            ),
            'state': 'unauth',
        })

    def get(self, request, *args, **kwargs):
        """Авторизация пользователя
           в заголовках необхожимо указать bearer 9c67...
           """
        # из текущей сессии получаем юзера и токен
        user = request.user
        token = request.token

        if user and user.is_authenticated:
            return self.get_auth_response(user)
        else:
            esia_data = token.esia_auth_data or {}
            if esia_data.get('orgs', None) is not None:
                companies = []
                if esia_data.get('companies', None):
                    token.set_login_code()

                    for company in esia_data.get('companies', None):
                        company['url_follow'] = '%s?%s' % (
                            reverse('esia-org', kwargs={'org_id': company['oid']}),
                            'login_code=%s' % (token.login_code)
                        )
                        companies.append(company)
                return JsonResponse({
                    'first_name': esia_data.get('first_name', None),
                    'last_name': esia_data.get('last_name', None),
                    'email': esia_data.get('email', None),
                    'orgs': esia_data.get('orgs', None),
                    'companies': companies,
                    'state': 'wait',
                })
            else:
                return self.get_esia_login_response()

    def post(self, request, *args, **kwargs):
        data = request.POST if request.POST else json.loads(request.body)
        username = data.get('username')
        password = data.get('password')

        try:
            if username and password:
                user = authenticate(request=request, username=username, password=password)
                if not user:
                    return return_exeption('Не удалось войти с указанными данными')
            else:
                return return_exeption('Не указаны логин или пароль')
        except AuthException as e:
            return self.get_esia_login_response({
                'code': e.args[0],
                'text': e.args[1],
            })
        # установим юзера в токен
        request.token.set_user(user)
        return self.get_auth_response(user)


class UserView(UserRequiredMixin, ErrorResponseMixin, View):

    def get(self, request, *args, **kwargs):
        user = request.user
        org = user.company

        return JsonResponse({
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'roles': list(user.roles.all().values_list('slug', flat=True)),
            'company': {
                'id': org.id,
                'name_full': org.name_full,
                'name_short': org.name_short,
                'inn': org.inn,
                'kpp': org.kpp,
                'ogrn': org.ogrn,
                'contacts': org.contacts,
            },
        })


class LogoutView(TokenRequiredMixin, ErrorResponseMixin, APIView):

    def get(self, request, *args, **kwargs):
        """
        """
        if request.token:
            request.token.delete()

        user = request.user
        if user and user.is_authenticated:
            logout(request)

        return JsonResponse({
            'status': True,
        })
