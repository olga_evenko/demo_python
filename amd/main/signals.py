from django.db.models.signals import post_save, post_delete
from ..main import Config
from ..main import cfg


# слушаем изменение конфига
def cfg_change(*args, **kwargs):
    cfg.preinit()


post_save.connect(cfg_change, sender=Config)
post_delete.connect(cfg_change, sender=Config)


post_save.connect(cfg_change, sender=Config)
post_delete.connect(cfg_change, sender=Config)

