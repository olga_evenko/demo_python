from django.urls import path

from .views import *


urlpatterns = [
    path('token/', TokenView.as_view()),
    path('token-refresh/', TokenRefreshView.as_view()),
    path('auth/', AuthView.as_view()),
    path('token-auth/', TokenAuthView.as_view()),
    path('logout/', LogoutView.as_view()),
]

