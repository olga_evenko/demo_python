from django.utils.timezone import now
from django.utils.cache import patch_vary_headers

from model_app.users.models import Token


def get_token_from_header(request):
    token = None

    if 'HTTP_AUTHORIZATION' in request.META:
        split_header = request.META.get('HTTP_AUTHORIZATION').split()
        if len(split_header) == 2 and split_header[0].lower() == 'bearer':
            token = split_header[1]

    return token


class AuthTokenMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        request.token = None
        # если юзер не установлен
        if not hasattr(request, 'user') or request.user.is_anonymous:
            # если удалось получить токен из заголовков
            token_label = get_token_from_header(request)
            # пробуем найти нужный токен
            token = Token.objects.filter(token=token_label, token_expire__gte=now()).first()
            if token:
                request.token = token
                request.user = token.user

        response = self.get_response(request)
        patch_vary_headers(response, ("Authorization",))
        return response
