from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.db import models


class TypeCompany(models.Model):
    TYPE = (
        ('carrier_company', 'Компания перевозчик'),
        ('reg_operator', 'Региональный оператор'),
        ('landfill_operator', 'Оператор объекта размещения'),
        ('roiv', 'РОИВ'),
        ('rpn', 'РПН'),
        ('omsu', 'Орган местного самоуправления'),
        ('emitter', 'Отходообразователь'),
    )
    type = models.CharField('Тип организации', max_length=25, choices=TYPE, null=True, blank=True)
    name = models.CharField('Наименование', max_length=255, null=True, blank=True)

    class Meta:
        verbose_name = 'Тип компании'
        verbose_name_plural = 'Типы компаний'
        db_table = 'type_company'

    def __str__(self):
        return str(self.name) or ''


class Role(models.Model):
    type_company = models.ForeignKey(TypeCompany, on_delete=models.CASCADE, related_name='roles', null=True)
    name = models.CharField('Наименование', max_length=255)
    description = models.TextField('Описание', null=True, blank=True)
    guest = models.BooleanField('Гостевая роль', default=True)
    admin = models.BooleanField('Администратор', default=False)
    user = models.ManyToManyField(User, related_name='roles')

    class Meta:
        verbose_name = 'Роль'
        verbose_name_plural = 'Роли'
        db_table = 'role'

    def __str__(self):
        return self.name


class Rule(models.Model):
    TYPE_ACTION = (
        ('post', 'create'),
        ('get', 'read'),
        ('put', 'update(put)'),
        ('patch', 'update(patch)'),
        ('delete', 'delete'),
    )

    role = models.ManyToManyField(Role, related_name='rules')
    table = models.CharField('Таблица', max_length=255, null=True, blank=True)
    action = models.CharField('Действие', max_length=25, choices=TYPE_ACTION, null=True, blank=True)
    exclude = models.BooleanField(default=False,)
    regulation = JSONField(null=True, blank=True)

    class Meta:
        verbose_name = 'Правило'
        verbose_name_plural = 'Правила'
        db_table = 'rule'
        unique_together = [('table', 'action', 'regulation')]

    def __str__(self):
        return '%s %s %s' %(self.table, self.action, self.regulation)
