import datetime
from access_management_django.rules.models import Rule
from django.contrib.auth.mixins import AccessMixin
from django.db.models import Q
from django.http import JsonResponse

from statistic.models import now


class UnauthException(Exception):
    pass


def return_exeption(text="Недостаточно прав", code=403):
    return JsonResponse(
        {
            'error_fields': None,
            'error_description': text,
        }
        , status=code)


class CheckPermissionOnlyAuth(AccessMixin):
    @staticmethod
    def filter_by_perm(user, queryset, method, basename):
        rules = Rule.objects.all().filter(role__in=user.roles.all(),
                                          action=method,
                                          table=basename)
        if not rules.count():
            return []

        q_objects = Q()
        can_get_all = False
        for r in rules:
            regulations = r.regulation

            if not regulations:
                can_get_all = True
                break
            try:
                q_objects_reg = Q()
                if not type(regulations) == list:  # если правило словарь, то преодразуем в список
                    regulations = [regulations]
                for regulation in regulations:
                    regulation_result = {}
                    exclude = False

                    for reg in regulation:
                        if reg == 'exclude':
                            if regulation[reg]:
                                exclude = True
                        else:
                            val = user
                            list_field = str(regulation[reg]).split('__')

                            list_models = {}
                            for field in list_field:
                                val = getattr(val, field, None)
                                list_models[field] = val
                            if getattr(val, 'model', None):
                                qs_filter_field, qs_filter_list = None, None
                                if reg.find('__id') > 0:
                                    regulation_result[reg + '__in'] = val.all()
                                else:
                                    for field in str(reg).split('__')[::-1]:
                                        if qs_filter_field:
                                            if list_models.get(field, None):
                                                qs_filter_list = list_models[field]._meta.model.objects.filter(
                                                    **{f'{qs_filter_field}__in': qs_filter_list}).distinct()
                                        else:
                                            qs_filter_list = val.all()
                                        qs_filter_field = field

                                    regulation_result[qs_filter_field + '__in'] = qs_filter_list
                            else:
                                if val:
                                    regulation_result[reg] = val
                                else:
                                    if field in ['True', 'true']:
                                        field = True
                                    if field in ['False', 'false']:
                                        field = False
                                    regulation_result[reg] = field

                        # обработываем lookup __active в левой части правила
                        reg_without_active = str(reg).split('__active')
                        if reg_without_active[0] != reg:  # парсим правило с __active
                            regulation_result[reg_without_active[0] + reg_without_active[1]] = regulation_result.pop(reg)
                            regulation_result[reg_without_active[0] + '__date_end__gt'] = now() + datetime.timedelta(
                                hours=3)

                    if exclude:
                        q_objects_reg |= ~Q(**regulation_result)
                    else:
                        q_objects_reg |= Q(**regulation_result)
            except Exception as e:
                return []

            q_objects |= Q(q_objects_reg)

        if not can_get_all:
            queryset = queryset.filter(q_objects)
        return queryset

    def dispatch(self, request, *args, **kwargs):
        try:
            if not request.user or not request.user.is_authenticated:
                if request.method.lower() != 'get':
                    return return_exeption(text="Доступ только для авторизованных пользователей", code=401)

            elif not request.user.is_superuser:
                rules = Rule.objects.all().filter(role__in=request.user.roles.all(),
                                                  action=request.method.lower(),
                                                  table=self.basename)
                if not rules.count():
                    return return_exeption(text="У пользователя недостаточно прав", code=403)

                if not hasattr(self, 'queryset'):
                    self.queryset = self.get_queryset()
                self.queryset = self.filter_by_perm(request.user, self.queryset, request.method.lower(), self.basename)

            return super().dispatch(request, *args, **kwargs)

        except UnauthException as e:
            return return_exeption()
        except Exception as e:
            return return_exeption('Внутренняя ошибка сервера', 500)


class CheckPermission(CheckPermissionOnlyAuth):

    def dispatch(self, request, *args, **kwargs):
        if not request.user or not request.user.is_authenticated:
            return return_exeption()

        return super().dispatch(request, *args, **kwargs)
