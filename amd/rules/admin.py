from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth.admin import UserAdmin
from django.forms import CheckboxSelectMultiple
from django.http import HttpResponseRedirect
from django.utils.html import format_html

from big3_data_main_app.settings import ROLES_DICT, REGULATIONS_DICT, SCOPES, SPREADSHEET_ID
from model_app.users.models import Profile
from .models import *

admin.site.unregister(User)


class ProfileInline(admin.StackedInline):
    list_select_related = (
        'company__region', 'company__region'
    )
    fields = ('company',)
    autocomplete_fields = ('company',)
    model = Profile
    extra = 1


class GroupInline(admin.TabularInline):
    verbose_name = "Роль пользователя"
    verbose_name_plural = "Роли пользователя"
    model = User.roles.through
    extra = 0

admin.site.register(TypeCompany)

@admin.register(User)
class CustomUserAdmin(UserAdmin):

    class Media:
        js = ('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js',
              '/static/js/scripts.js')

    def get_inlines(self, request, obj=None):
        if obj:
            return [ProfileInline, GroupInline]
        else:
            return []

    def login_as(self, obj):
        html_string = f'<input type="button" value="Войти" onclick="loginAs(\'{obj.username}\')">'
        return format_html(html_string)

    fieldsets = (
        ('Personal info', {'fields': ('username', 'first_name', 'last_name', 'email', 'password')}),
        ('Important dates', {'fields': ('date_joined',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')}),
    )

    login_as.short_description = 'Войти под пользователем'
    list_display = (
        'username',
        'email',
        'first_name',
        'last_name',
        'is_staff',
        'login_as'
    )


class RuleInline(admin.TabularInline):
    list_select_related = (
        'role__type_company',
    )
    verbose_name = "Разрешение"
    verbose_name_plural = "Разрешения"
    model = Role.rules.through
    extra = 0


@admin.register(Role)
class CustomRoleAdmin(ModelAdmin):
    def get_inlines(self, request, obj=None):
        if obj:
            return [RuleInline, ]
        else:
            return []

    fieldsets = (
        ('Базовая информация', {'fields': ('type_company', 'name', 'description')}),
        ('Типы роли', {'fields': ('guest', 'admin')}),
        ('Пользователи', {'fields': ('user',)}),
    )


@admin.register(Rule)
class RuleAdmin(admin.ModelAdmin):
    list_filter = ('role',)   # фильтр по ролям
    # путь к шаблону в /templates/
    change_list_template = 'admin/rules/rule/change_list.html'

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            url('load-rules/', self.admin_site.admin_view(self.load_rules),
                name='rules_load_rules'),
        ]
        return my_urls + urls

    def load_rules(self, request):
        import pickle
        import os.path
        from googleapiclient.discovery import build
        from google_auth_oauthlib.flow import InstalledAppFlow
        from google.auth.transport.requests import Request

        rules = list()      # список правил из гугл-таблицы
        '''
        Формат правила:
            {
                'table': '',
                'action': '',
                'regulation': {},
                'roles': [],
            }
        '''

        role_objects = dict()   # словарь объектов Роль(Role) для добавления связей с Правилом(Rule)
        '''
        Формат роли:
            {'роль': Role.objects.get(name='роль')}
        '''

        METHODS = ['get', 'post', 'put', 'patch', 'delete']

        def get_role(title):
            role_rus = title.split(':')[-1].strip()
            if role_rus in ROLES_DICT:
                return ROLES_DICT[role_rus]
            else:
                return None

        def get_regulation(rule_rus, table):
            regulation = None
            if rule_rus in REGULATIONS_DICT:
                if table in REGULATIONS_DICT[rule_rus]:
                    regulation = REGULATIONS_DICT[rule_rus][table]
                else:
                    regulation = REGULATIONS_DICT[rule_rus]['all']
            return regulation

        def record_rules(main_row, role):
            table = main_row[0]
            # Отделяем правила для <роль> и <роль>_admin. Дублируем колонку "Редактирование" для put и patch
            rows = [
                {'role': str(role) + '_admin', 'rules': main_row[2:5] + main_row[4:6]},
                {'role': role, 'rules': main_row[7:10] + main_row[9:11]},
            ]
            for row in rows:
                for i, rule in enumerate(row['rules']):
                    regulation = get_regulation(rule, table)
                    if regulation:
                        # проверяем наличие словаря с такими table, action, rule
                        exist_rules = list(filter(lambda r:
                                                  r['table'] == table and
                                                  r['action'] == METHODS[i] and
                                                  r['regulation'] == regulation, rules))
                        if len(exist_rules) == 0:   # такого правила еще не было
                            rec = {
                                'table': table,
                                'action': METHODS[i],
                                'regulation': get_regulation(rule, table),
                                'roles': [row['role']],
                            }
                            rules.append(rec)
                        else:
                            # Такое правило уже загружено. Проверяем, есть ли текущая роль
                            exist_roles = exist_rules[0]['roles']
                            if row['role'] not in exist_roles:
                                exist_rules[0]['roles'].append(row['role'])

        def get_role_object(role):
            if role in role_objects:
                role_object = role_objects[role]
            else:
                role_object = Role.objects.filter(name=role).first()
                role_objects[role] = role_object  # добавим в список полученный объект
            return role_object

        def add_roles(rule, roles):
            roles_for_adding = list()
            for role in roles:
                role_obj = get_role_object(role)
                roles_for_adding.append(role_obj)
            rule.role.clear()   # удаляем старые связи
            for adding in roles_for_adding:
                if adding:
                    rule.role.add(adding)

        def clear_roles(rule, roles):
            roles_for_clearing = list()
            for role in roles:
                role_obj = get_role_object(role)
                roles_for_clearing.append(role_obj)
            rule.role.remove(*roles_for_clearing)

        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'external_integrations/credentials.json', SCOPES)
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        service = build('sheets', 'v4', credentials=creds)
        # Call the Sheets API
        sheet = service.spreadsheets()

        # Get sheets info
        sheet_metadata = sheet.get(spreadsheetId=SPREADSHEET_ID).execute()
        sheets_info = sheet_metadata.get('sheets', '')
        for sheet_info in sheets_info:
            sheet_title = sheet_info['properties']['title']
            if 'Роль:' in sheet_title:
                role = get_role(sheet_title)
                range_name = f'{sheet_title}!A4:K24'    # 20 строк (если строк будет больше, увеличить это значение)
                result = sheet.values().get(spreadsheetId=SPREADSHEET_ID, range=range_name).execute()
                values = result.get('values', [])
                for row in values:
                    if row[0] != '':
                        record_rules(row, role)
                    else:
                        break

        # сохраняем полученные записи в БД
        num_new, num_changed, num_deleted = 0, 0, 0
        for r in rules:
            new_roles = set(r['roles'])
            if r['regulation'] == 'no permits':
                qs = Rule.objects.filter(table=r['table'], action=r['action'])
                for rule in qs:
                    roles_from_object = set(rule.role.all().values_list('name', flat=True))
                    roles_for_delete = new_roles & roles_from_object    # пересечение множеств
                    if new_roles == roles_from_object:  # все роли к удалению
                        rule.delete()
                        num_deleted += 1
                    elif roles_for_delete:
                        clear_roles(rule, roles_for_delete)
                        num_changed += 1
            else:
                if r['regulation']:
                    rule = Rule.objects.filter(table=r['table'], action=r['action'], regulation=r['regulation']).first()
                else:
                    rule = Rule.objects.filter(table=r['table'], action=r['action'], regulation__isnull=True).first()
                if rule:
                    roles_from_object = set(rule.role.all().values_list('name', flat=True))
                    if new_roles != roles_from_object:
                        add_roles(rule, new_roles)
                        num_changed += 1
                else:
                    rule = Rule.objects.get_or_create(table=r['table'], action=r['action'], regulation=r['regulation'])[0]
                    rule.save()
                    add_roles(rule, new_roles)
                    num_new += 1

        self.message_user(request, f'Обработано {len(rules)} правил. Добавлено {num_new} новых правил. '
                                   f'Удалено {num_deleted} правил. Изменено {num_changed} правил.')
        return HttpResponseRedirect('../')
