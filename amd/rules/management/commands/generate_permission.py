from django.core.management.base import BaseCommand
from rules.models import Rule, TypeCompany, Role


class Command(BaseCommand):

    def handle(self, *args, **options):

        def perm_create(role, action, mod):
            if role.type_company:
                if role.type_company.type == 'reg_operator':
                    if mod == 'company':
                        rule = Rule.objects.update_or_create(
                            table=mod,
                            action=action,
                            regulation={"as_contractor__client__id": "company_id"}
                        )
                        return rule[0]
                if role.type_company.type == 'carrier_company':
                    if mod == 'company':
                        rule = Rule.objects.update_or_create(
                            table=mod,
                            action=action,
                            regulation={"as_client__contractor__id": "company_id"}
                        )
                        return rule[0]
                    if mod == 'transport':
                        rule = Rule.objects.update_or_create(
                            table=mod,
                            action=action,
                            regulation={"owner__id": "company_id"}
                        )
                        return rule[0]
                if role.type_company.type == 'roiv':
                    if mod == 'wastesite':
                        rule = Rule.objects.update_or_create(
                            table=mod,
                            action=action,
                            regulation={"owner__id": "company_id"}
                        )
                        return rule[0]
                if role.type_company.type == 'landfill_operator':
                    if mod == 'landfill':
                        rule = Rule.objects.update_or_create(
                            table=mod,
                            action=action,
                            regulation={"owner__id": "company_id"}
                        )
                        return rule[0]

            rule = Rule.objects.filter(table=mod, action=action, regulation__isnull=True).first()
            if not rule:
                rule = Rule.objects.update_or_create(
                    table=mod,
                    action=action,
                    regulation=None
                )[0]
            return rule

        types_company = TypeCompany.objects.all()
        for tc in types_company:
            Role.objects.update_or_create(
                name='%s_admin' % tc.type,
                type_company=tc,
                admin=True,
                guest=False
            )
            Role.objects.update_or_create(
                name='%s' % tc.type,
                type_company=tc,
                guest=False
            )

        Role.objects.update_or_create(
            name='admin',
            admin=True,
            guest=False
        )
        Role.objects.update_or_create(
            name='guest',
            guest=True
        )

        # models = apps.get_models()
        models = ['company', 'transport', 'wastesite', 'landfill']
        for m in models:
            for action in ['get', 'post', 'patch', 'put', 'delete']:
                roles = Role.objects.all()
                roles_admin = roles.filter(admin=True)
                for r in roles_admin:

                    rule = Rule.objects.filter(table=m, action=action, regulation__isnull=True).first()
                    if not rule:
                        rule = Rule.objects.update_or_create(
                            table=m,
                            action=action,
                            regulation=None
                        )[0]
                    r.rules.add(rule)
                roles_no_admin = roles.filter(admin=False)
                for r in roles_no_admin:
                    rule = perm_create(r, action, m)
                    r.rules.add(rule)
