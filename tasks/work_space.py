import json
import math
from math import isnan

from celery import current_task
from django.core.exceptions import FieldDoesNotExist
from openpyxl.styles import Protection
from openpyxl.worksheet.datavalidation import DataValidation

from model_app.waste.models import Container


def update_state(state, meta=None):
    try:
        if current_task:
            current_task.update_state(state=state, meta=meta)
    except Exception:
        pass


def get_data_query(model, serializer_class, param=[]):
    if param.get('data_list', None) is not None:
        qs = model.objects.filter(id__in=param.get('data_list', []))
    else:
        qs = model.objects.all()
    serializer = serializer_class(qs, many=True).data
    return serializer


def unlocked_cell(row, col, sheet):
    for r in range(3, row + 100):
        for c in range(1, col + 20):
            cell = sheet.cell(r, c)
            cell.protection = Protection(locked=False)


def write_data_to_xlsx(row, serializer_class, d, sheet, model, dv_dict, meta=None):
    if meta:
        meta['current'] += 1
        if (row - 3) % 100 == 0:
            update_state(state='PROGRESS', meta=meta)
    col = 1
    for column in serializer_class().fields.fields:
        if d.get(column):

            value = d[column]
            if type(d[column]) == list:
                value = json.dumps(value)
            if type(d[column]) == dict:
                value = json.dumps(value)
            if type(d[column]) == bool:
                value = 'Да' if value else 'Нет'
            cell = sheet.cell(row=row, column=col, value=value)
            if getattr(model, column, None) and getattr(model, column, None).field.choices:
                if not dv_dict.get(column):
                    arr_many_to_one = ','.join(str(res[1]) for res in getattr(model, column).field.choices)
                    dv_dict[column] = DataValidation(type="list", formula1=f'"{arr_many_to_one}"', allow_blank=True)
                dv_dict[column].add(cell)
            if type(d[column]) == bool:
                if not dv_dict.get(column):
                    dv_dict[column] = DataValidation(type="list", formula1=f'"Да,Нет"', allow_blank=True)
                dv_dict[column].add(cell)
        col += 1

    if not dv_dict.get('delete'):
        dv_dict['delete'] = DataValidation(type="list", formula1='"Да,Нет"', allow_blank=True)
    cell = sheet.cell(row=row, column=col, value=None)
    dv_dict['delete'].add(cell)


def body_doc(param, meta, qs=None, wb=None):
    sheet = param['sheet']
    model = param['model']
    serializer_class = param['serializer_class']
    dv_dict = {}
    row = 3

    if param.get('data', None) is not None:
        serializer_data = param['data']
        for d in serializer_data:
            write_data_to_xlsx(row, serializer_class, d, sheet, model, dv_dict)
            row += 1
    else:
        max_id = getattr(qs.last(), 'id', 0)
        min_id = getattr(qs.first(), 'id', 0)

        limit = param['limit'] if (param.get('limit') > 0 and param.get('limit') < 5000) else 5000

        while min_id < max_id and (row - 3 < param['limit'] if param.get('limit') > 0 else True):

            qs_part = qs.filter(id__gte=min_id)[:limit]
            serializer_data = serializer_class(qs_part, many=True).data

            for d in serializer_data:
                if row - 3 > param['limit'] if param.get('limit') > 0 else False:
                    break
                write_data_to_xlsx(row, serializer_class, d, sheet, model, dv_dict, meta)
                row += 1
            for p in param['temp']:
                p['row'] = p.get('row', 3)

                model_objects = p['model'].objects
                if p['prefetch_related']:
                    model_objects = model_objects.prefetch_related(*p['prefetch_related'])
                container = model_objects.filter(**{
                    f"{p['fk_sheet']}__id__in": qs_part.values_list('id', flat=True)
                })
                serializer_data = p['serializer_class'](container, many=True).data
                for s_d in serializer_data:
                    write_data_to_xlsx(p['row'], p['serializer_class'], s_d, p['sheet'], p['model'], dv_dict)
                    p['row'] += 1

            min_id = d['id']

        update_state(state='PROGRESS', meta=meta)

    for dv in dv_dict:
        sheet.add_data_validation(dv_dict[dv])
    unlocked_cell(row, 0, sheet)


def head_doc(param):
    sheet = param['sheet']
    model = param['model']
    serializer_class = param['serializer_class']
    col = 1
    for column in serializer_class().fields.fields:
        try:
            sheet.cell(row=1, column=col, value=model._meta.get_field(column).verbose_name)
        except:
            if getattr(serializer_class().fields.fields[column], 'label_ru', None):
                sheet.cell(row=1, column=col, value=getattr(serializer_class().fields.fields[column], 'label_ru'))
        sheet.cell(row=2, column=col, value=column)
        col += 1
    if param.get('delete'):
        sheet.cell(row=1, column=col, value='Удаление')
        sheet.cell(row=2, column=col, value='delete')


def convert_data(data, model, param={}, parent_dict={}):
    for d in data:
        if type(data[d]) == float and math.isnan(data[d]):
            data[d] = None
        try:
            if data.get(d) and d in ['lat', 'lon']:
                data[d] = round(float(data[d]), 6)
            elif type(data[d]) == float and isnan(data[d]):
                data[d] = None
            elif type(data[d]) == float and int(data[d]) == data[d]:
                data[d] = int(data[d])
            elif d == 'delete' or model._meta.get_field(d) and model._meta.get_field(
                    d).get_internal_type() == 'BooleanField':
                if data[d] == 'Да':
                    data[d] = True
                if data[d] == 'Нет':
                    data[d] = False
            elif data[d] and model._meta.get_field(d) and model._meta.get_field(d).get_internal_type() == 'DateTimeField':
                data[d] = data[d].replace('Z', '')

            if d in list(parent_dict.keys()):
                # присваиваем внутренний id из массива соответствия внешнего id и внутреннего id
                data[d] = parent_dict[d].get(data[param['parent'][d][1]], None)
        except FieldDoesNotExist as e:
            pass
        except Exception as e:
            pass
