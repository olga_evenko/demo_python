from django.http import JsonResponse
from celery.result import AsyncResult
from rest_framework.views import APIView


class Tasks(APIView):

    def get(self, request, pk):
        task_result = AsyncResult(pk)
        result = {
            "task_id": pk,
            "task_status": task_result.status,
            "task_result": task_result.result
        }
        return JsonResponse(result, status=200)