# from celery import shared_task, app
import datetime
import io
from itertools import islice
import json
import math
from math import isnan

import openpyxl
import pandas as pd
from celery.exceptions import Ignore
from django.apps import apps
from openpyxl.writer.excel import save_virtual_workbook
from pandas import DataFrame

from access_management_django.rules.views import CheckPermissionOnlyAuth
from big3_data_main_app.celery import app
from model_app.base.models import User, Company
from model_app.cabinet.models import ExelFile
from statistic.serializers import WasteSiteExelSerializer, ContainerExelSerializerSave
from statistic import serializers
from model_app.waste.models import WasteSite, Container
from statistic import serializers
from statistic.serializers import WasteSiteExelSerializer, ContainerExelSerializerSave
from tasks.work_space import head_doc, body_doc, convert_data, update_state
from datetime import datetime


def generate_params(wb, p, sheet_item=0):
    p['model'] = apps.get_model(app_label=p['app_label'], model_name=p['model_name'])
    p['serializer_class'] = getattr(serializers, p['serializer_class_name'], None)
    sheets = wb.create_sheet(p['model']._meta.db_table, sheet_item)

    params_data = {
        'sheet': sheets,
        'serializer_class': p['serializer_class'],
        'model': p['model'],
        'delete': True if p.get('delete') else False,
        'filter': p.get('filter', {}),
        'limit': p.get('limit', -1),
        'order_by': p.get('order_by', 'id'),
        'fk_sheet': p.get('fk_sheet', None),
        'temp': [generate_params(wb, item) for item in p.get('temp', [])],
        'prefetch_related': p.get('prefetch_related', None)}
    return params_data


# celery -A big3_data_main_app worker --pool=solo --loglevel=INFO --concurrency=2
@app.task
def generate_xlsx(filename, params, auth_params=None):
    try:
        wb = openpyxl.Workbook()
        sheet_item = 1

        meta = {
            'total': None,
            'current': 0,
        }
        update_state(state='PROGRESS', meta=meta)
        params_data_list = []
        for p in params:

            params_data = generate_params(wb, p, sheet_item)

            model_objects = params_data['model'].objects
            if params_data['prefetch_related']:
                model_objects = model_objects.prefetch_related(*params_data['prefetch_related'])
            qs = model_objects.filter(**params_data['filter']).order_by(params_data.get('order_by', 'id'))
            if auth_params:
                user = User.objects.get(id=auth_params.get('user_id', None))
                basename = auth_params.get('basename_model', None)
                qs = CheckPermissionOnlyAuth.filter_by_perm(user, qs, 'get', basename)

            count = qs.count()
            if p.get('limit', -1) > 0 and count >= p.get('limit'):
                count = p.get('limit')

            meta['total'] = count + (meta['total'] if meta['total'] else 0)
            params_data_list.append({
                'params_data': params_data,
                'qs': qs
            })
        update_state(state='PROGRESS', meta=meta)
        for params_data in params_data_list:
            # sheets.protection.sheet = True
            head_doc(params_data['params_data'])
            for pp in params_data['params_data'].get('temp', []):
                head_doc(pp)
            body_doc(params_data['params_data'], meta, params_data['qs'], wb)
            sheet_item += 1
        wb.close()
        exel_file = ExelFile.objects.create(name=f'{filename}.xlsx')

        try:
            exel_file.file.save(f'{filename}.xlsx', io.BytesIO(save_virtual_workbook(wb)))
            return meta
        except Exception as e:
            update_state(state='ERROR', )
            print(e)
            raise Ignore()

    except Exception as e:
        update_state(state='ERROR', )
        print(e)
        raise Ignore()


@app.task
def read_doc(pd_read, params, filename):
    wb_new = openpyxl.Workbook()
    sheet_item = 1
    for p in params:
        p['model'] = apps.get_model(app_label=p['app_label'], model_name=p['model_name'])
        p['serializer_class'] = getattr(serializers, p['serializer_class_name'], None)
        p['parent'] = p.get('parent', None)

    meta_result = {
        'total': 0,
        'current': 0,
        'inserted': 0,
        'updated': 0,
        'deleted': 0,
    }
    update_state(state='PROGRESS', meta=meta_result)
    for sh in pd_read['sheet_name']:
        dfs = pd.read_excel(pd_read['url'], header=pd_read['header'],
                            sheet_name=sh)
        meta_result['total'] = len(dfs.to_dict('record')) + (meta_result['total'] if meta_result['total'] else 0)
        update_state(state='PROGRESS', meta=meta_result)

    for sh in pd_read['sheet_name']:
        param = next(item for item in params if item["model"]._meta.db_table == sh)
        try:
            dfs = pd.read_excel(pd_read['url'], header=pd_read['header'],
                                sheet_name=sh)
            parent_dict = {}
            if param['parent']:
                for fk_obj in param['parent']:
                    index = param['parent'][fk_obj]
                    data = wb_new.get_sheet_by_name(fk_obj).values
                    next(data)
                    cols = next(data)
                    data = (islice(r, 0, None) for r in list(data))
                    df = DataFrame(data, columns=cols)
                    data = df.to_dict('record')
                    parent_dict[fk_obj] = {item[index[0]]: item[index[2]] for item in data if type(item[index[0]])}

            sheets_new = wb_new.create_sheet(param['model']._meta.db_table, sheet_item)
            sheet = dfs.to_dict('record')
            obj_list = []

            head_doc(
                {'sheet': sheets_new, 'serializer_class': param['serializer_class'], 'model': param['model']})

            body_doc(meta=meta_result, param={
                'sheet': sheets_new,
                'serializer_class': param['serializer_class'],
                'model': param['model'],
                'data': sheet}
                     )
            row = 3

            for data in sheet:
                meta_result['current'] += 1
                if (row - 3) % 100 == 0:
                    update_state(state='PROGRESS', meta=meta_result)

                convert_data(data, param['model'], param, parent_dict)
                col = len(data.keys()) + 1
                row += 1

                try:
                    if data.get('id', None) and not isnan(data.get('id')):
                        try:
                            obj = param['model'].objects.get(id=data.get('id'))
                        except param['model'].DoesNotExist:
                            obj = None
                    else:
                        obj = None
                        for unique_together in param['model']._meta.original_attrs['unique_together']:
                            if all([True if data.get(item) else False for item in unique_together]) and not obj:
                                filter_qs = {item: data.get(item) for item in unique_together}
                                try:
                                    obj = param['model'].objects.get(**filter_qs)
                                except param['model'].DoesNotExist:
                                    pass

                    if data.get('delete', None) and data.get('delete') > 0:
                        if obj:
                            obj.delete()
                            result = 'Удалено'
                            meta_result['deleted'] += 1
                        else:
                            result = 'Объект не найден'
                    else:
                        if obj:
                            serializer = param['serializer_class'](obj, data=data)
                        else:
                            serializer = param['serializer_class'](data=data)

                        if serializer.is_valid():
                            if obj in obj_list:
                                result = 'Ошибка. Повторное сохранение.'
                            else:
                                obj_list.append(obj)
                                serializer.save()
                                result = 'Обновлено' if obj else 'Добавлено'
                                meta_result['updated' if obj else 'inserted'] += 1
                        else:
                            result = ''
                            for e in serializer.errors:
                                result += f'{e}: {serializer.errors[e][0].title()} '
                    sheets_new.cell(row=row - 1, column=col, value=result)
                    if serializer.is_valid():
                        sheets_new.cell(row=row - 1, column=list(data.keys()).index('id')+1, value=serializer.data['id'])
                    sheet_item += 1
                except Exception:
                    sheets_new.cell(row=row - 1, column=col, value='Ошибка при сохранении')

        except Exception as e:
            update_state(state='ERROR', )
            raise Ignore()

    wb_new.close()
    exel_file = ExelFile.objects.create(name=f'{filename}.xlsx')
    try:
        exel_file.file.save(f'{filename}.xlsx', io.BytesIO(save_virtual_workbook(wb_new)))
        return meta_result
    except Exception as e:
        update_state(state='ERROR', )
        raise Ignore()


@app.task
def save_ws(sheet_ws, sheet_c, request_data):
    sheet_ws = json.loads(sheet_ws)
    sheet_c = json.loads(sheet_c)
    meta_result = {
        'mno': 0,
        'container': 0,
    }
    update_state(state='PROGRESS', meta=meta_result)
    try:
        lat_label = request_data.get('lat', 'lat')
        lon_label = request_data.get('lon', 'lon')
        address_label = request_data.get('address', 'address')
        ext_id_label = request_data.get('ext_id', 'ext_id')

        owner_inn_label = request_data.get('owner_inn', 'owner_inn')
        owner_ogrn_label = request_data.get('owner_ogrn', 'owner_ogrn')
        owner_name_label = request_data.get('owner_name', 'owner_name')

        schedule_label = request_data.get('schedule', 'schedule')
        number_label = request_data.get('number', 'number')
        capacity_label = request_data.get('capacity', 'capacity')

        ws_save_object = {}

        for s_ws in sheet_ws:
            lat = round(s_ws.get(lat_label, 0), 6)
            lon = round(s_ws.get(lon_label, 0), 6)
            address = s_ws.get(address_label, None)
            ext_id = s_ws.get(ext_id_label, None)

            owner_inn = s_ws.get(owner_inn_label, None)
            owner_ogrn = s_ws.get(owner_ogrn_label, None)
            owner_name = s_ws.get(owner_name_label, None)
            schedule = str(s_ws.get(schedule_label, None))

            company = Company.objects.get_or_create(ogrn=owner_ogrn,
                                                    defaults={
                                                        'inn': owner_inn,
                                                        'name': owner_name, })

            ws = WasteSite.objects.get_or_create(
                lat=lat, lon=lon,
                defaults={
                    'address': address,
                    'company': company[0],
                    'owner': company[0],
                    'date_mount': datetime.datetime.now()
                }
            )[0]

            ws_data = {
                'schedule': schedule,
            }
            ws_serializer = WasteSiteExelSerializer(ws, data=ws_data, partial=True)

            if ws_serializer.is_valid():
                ws_serializer.save()

            ws_save_object[ext_id] = ws.id

            meta_result['mno'] +=1
            update_state(state='PROGRESS', meta=meta_result)

        for s_c in sheet_c:
            owner_inn = s_c.get(owner_inn_label, None)
            owner_ogrn = s_c.get(owner_ogrn_label, None)
            owner_name = s_c.get(owner_name_label, None)

            ext_id = s_c.get(ext_id_label, None)
            number = s_c.get(number_label, None)
            number = int(number) if type(number) == float and not math.isnan(number) else number

            capacity = s_c.get(capacity_label, None)
            capacity = capacity if type(capacity) == float and not math.isnan(capacity) else None
            schedule = str(s_c.get(schedule_label, None))

            company = Company.objects.get_or_create(ogrn=owner_ogrn,
                                                    defaults={
                                                        'inn': owner_inn,
                                                        'name': owner_name, })

            ws = WasteSite.objects.get(id=ws_save_object[ext_id])
            c = Container.objects.get_or_create(
                waste_site=ws, number=number,
                defaults={
                    'company': company[0],
                    'owner': company[0],
                    'capacity': capacity
                }
            )[0]
            c_data = {
                'schedule': schedule,
            }
            c_serializer = ContainerExelSerializerSave(c, data=c_data, partial=True)
            if c_serializer.is_valid():
                c_serializer.save()

            meta_result['container'] += 1
            update_state(state='PROGRESS', meta=meta_result)
    except Exception:
        update_state(state='ERROR',  meta=meta_result)
        raise Ignore()

