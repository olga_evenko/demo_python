from django.urls import path

from tasks import views

urlpatterns = [
    path('get_status/<str:pk>/', views.Tasks.as_view()),
]